import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtMultimedia

ApplicationWindow {
    id: window
    width: 400
    height: 400
    visible: true
    MediaPlayer {
            id: mediaPlayer
            source: "qrc:/funny.mp3" // Replace with the actual sound file path
            audioOutput: AudioOutput {}
        }
    Button {
        text: "Open"
        onClicked: popup.open()
        Component.onCompleted: {
                    mediaPlayer.play()
                }
    }

   Popup {
        id: popup
        x: 100
        y: 100
        width: 200
        height: 300
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        Button {
            id:x
            width:15
            text: "X"
            anchors.right:parent.right
            onClicked: popup.close()
        }
    }


}
